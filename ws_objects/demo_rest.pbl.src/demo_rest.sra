﻿$PBExportHeader$demo_rest.sra
$PBExportComments$Generated Application Object
forward
global type demo_rest from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global type demo_rest from application
string appname = "demo_rest"
end type
global demo_rest demo_rest

on demo_rest.create
appname="demo_rest"
message=create message
sqlca=create transaction
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on demo_rest.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;Open(w_translate)
end event


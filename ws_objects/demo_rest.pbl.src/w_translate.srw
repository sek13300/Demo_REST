﻿$PBExportHeader$w_translate.srw
forward
global type w_translate from window
end type
type dw_status from datawindow within w_translate
end type
type p_logo from picture within w_translate
end type
type pb_translate from picturebutton within w_translate
end type
type cbx_autodetect from checkbox within w_translate
end type
type ddlb_to from dropdownlistbox within w_translate
end type
type st_to from statictext within w_translate
end type
type ddlb_from from dropdownlistbox within w_translate
end type
type st_from from statictext within w_translate
end type
type tab_translation from tab within w_translate
end type
type tabpage_trans_text from userobject within tab_translation
end type
type mle_trans_text from multilineedit within tabpage_trans_text
end type
type tabpage_trans_text from userobject within tab_translation
mle_trans_text mle_trans_text
end type
type tabpage_trans_headers from userobject within tab_translation
end type
type mle_trans_headers from multilineedit within tabpage_trans_headers
end type
type tabpage_trans_headers from userobject within tab_translation
mle_trans_headers mle_trans_headers
end type
type tabpage_trans_json from userobject within tab_translation
end type
type mle_trans_body from multilineedit within tabpage_trans_json
end type
type tabpage_trans_json from userobject within tab_translation
mle_trans_body mle_trans_body
end type
type tabpage_trans_bodydw from userobject within tab_translation
end type
type dw_trans_json_body from datawindow within tabpage_trans_bodydw
end type
type tabpage_trans_bodydw from userobject within tab_translation
dw_trans_json_body dw_trans_json_body
end type
type tabpage_trans_raw from userobject within tab_translation
end type
type mle_trans_raw from multilineedit within tabpage_trans_raw
end type
type tabpage_trans_raw from userobject within tab_translation
mle_trans_raw mle_trans_raw
end type
type tab_translation from tab within w_translate
tabpage_trans_text tabpage_trans_text
tabpage_trans_headers tabpage_trans_headers
tabpage_trans_json tabpage_trans_json
tabpage_trans_bodydw tabpage_trans_bodydw
tabpage_trans_raw tabpage_trans_raw
end type
type tab_origin from tab within w_translate
end type
type tabpage_origin_text from userobject within tab_origin
end type
type mle_origin_text from multilineedit within tabpage_origin_text
end type
type tabpage_origin_text from userobject within tab_origin
mle_origin_text mle_origin_text
end type
type tabpage_origin_header from userobject within tab_origin
end type
type mle_origin_headers from multilineedit within tabpage_origin_header
end type
type tabpage_origin_header from userobject within tab_origin
mle_origin_headers mle_origin_headers
end type
type tabpage_origin_body from userobject within tab_origin
end type
type mle_origin_body from multilineedit within tabpage_origin_body
end type
type tabpage_origin_body from userobject within tab_origin
mle_origin_body mle_origin_body
end type
type tabpage_origin_bodydw from userobject within tab_origin
end type
type dw_json_body from datawindow within tabpage_origin_bodydw
end type
type tabpage_origin_bodydw from userobject within tab_origin
dw_json_body dw_json_body
end type
type tabpage_origin_raw from userobject within tab_origin
end type
type mle_origin_raw from multilineedit within tabpage_origin_raw
end type
type tabpage_origin_raw from userobject within tab_origin
mle_origin_raw mle_origin_raw
end type
type tabpage_origin_api_key from userobject within tab_origin
end type
type sle_api_key from singlelineedit within tabpage_origin_api_key
end type
type st_api_key from statictext within tabpage_origin_api_key
end type
type tabpage_origin_api_key from userobject within tab_origin
sle_api_key sle_api_key
st_api_key st_api_key
end type
type tab_origin from tab within w_translate
tabpage_origin_text tabpage_origin_text
tabpage_origin_header tabpage_origin_header
tabpage_origin_body tabpage_origin_body
tabpage_origin_bodydw tabpage_origin_bodydw
tabpage_origin_raw tabpage_origin_raw
tabpage_origin_api_key tabpage_origin_api_key
end type
type gb_origin from groupbox within w_translate
end type
type gb_translation from groupbox within w_translate
end type
type gb_lang from groupbox within w_translate
end type
type p_source from picture within w_translate
end type
type p_translate from picture within w_translate
end type
end forward

global type w_translate from window
integer width = 5874
integer height = 2816
boolean titlebar = true
string title = "PB Translate"
boolean controlmenu = true
boolean minbox = true
long backcolor = 16777215
string icon = "C:\PB_Workspaces\2017R2\Demo_REST\images\logo1.ico"
boolean clientedge = true
boolean center = true
dw_status dw_status
p_logo p_logo
pb_translate pb_translate
cbx_autodetect cbx_autodetect
ddlb_to ddlb_to
st_to st_to
ddlb_from ddlb_from
st_from st_from
tab_translation tab_translation
tab_origin tab_origin
gb_origin gb_origin
gb_translation gb_translation
gb_lang gb_lang
p_source p_source
p_translate p_translate
end type
global w_translate w_translate

type variables
//************************************************************
//		Instance Variable Declarations and Initialization
//************************************************************
STRING			is_api_key			//	Google's API Key
STRING			is_lang_from		//	Language FROM which to Translate from
STRING			is_lang_to			//	Language TO Translate into
LONG				il_last_status_row	//	Last Status Row of the Status DW

//	Object Variables
HTTPCLIENT	inv_http				//	New HTTPClient Object
JSONPARSER	inv_jsonp			//	New JSONParser Object
JSONGenerator inv_jsong			// 	New JSONGenerator Object
RESTClient 		inv_restc				// 	New RESTClient Object
DATASTORE	idst_headers		//	Datastore in which to store the headers
DATAWINDOW	idw_origin_json	//	Datawindow Object Variable used to simplify the code
DATAWINDOW	idw_trans_json		//	Datawindow Object Variable used to simplify the code
end variables

forward prototypes
public function integer of_initial_requestheaders ()
public function integer of_addrequestheader (string as_header, string as_value)
public function integer of_setrequestheaders ()
public function integer of_supported_languages ()
public subroutine of_set_lang_from (string as_lang)
public subroutine of_set_lang_to (string as_lang)
public function string of_parsejson_lang_detect (string as_json)
public function string of_parsejson_translate (string as_json)
end prototypes

public function integer of_initial_requestheaders ();///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**  	Object:				w_translate
**		Type: 				Window
**		Function Name:	of_Initial_RequestHeaders()
**		Brief Description: 	Sets the Initial Request Headers
**  	Parent: 				Window
**		Created by:			Govinda Lopez
**  	Date Created:		Dec/21/2017
**		Modified by:	
**		Date Modified:
**  	Description:			This Function Sets the Initial Request Headers. More info on the RESTFul Web Service (Google Cloud Translation API v2):
**								https://developers.google.com/apis-explorer/?hl=en_US#p/translate/v2/
**
**		Parameters:			(None)
**           					
**		Returns:           	INTEGER
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************
//		Instance Variable Declarations and Initialization
//************************************************************
INTEGER	li_count	//	Gets the Number of Headers Added

//	Add the headers to get the supported languages
li_count = of_addrequestheader("target","en")
li_count = of_addrequestheader("model","nmt")
li_count = of_addrequestheader("key",is_api_key)

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Adding Initial Request Headers...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Return the number of headers added
RETURN li_count
end function

public function integer of_addrequestheader (string as_header, string as_value);///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**  	Object:				w_translate
**		Type: 				Window
**		Function Name:	of_AddRequestHeaders()
**		Brief Description: 	Adds the Request Headers
**  	Parent: 				Window
**		Created by:			Govinda Lopez
**  	Date Created:		Dec/21/2017
**		Modified by:	
**		Date Modified:
**  	Description:			This Function Adds the Request Headers. More info on the RESTFul Web Service (Google Cloud Translation API v2):
**								https://developers.google.com/apis-explorer/?hl=en_US#p/translate/v2/
**
**		Parameters:			STRING	as_header	//	Variable used to pass in the Header
**								STRING	as_value		//	Variable used to pass in the Value
**           					
**		Returns:           	INTEGER
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************
//		Instance Variable Declarations and Initialization
//************************************************************
INTEGER	li_index	//	Used to Return the number of the Row that was inserted into the DataStore



//************************************************************
//		Logic of the Script
//************************************************************
//	Insert a New Row into the DataStore
li_index = idst_headers.InsertRow(0)

//	Set's the Items
idst_headers.SetItem(li_index,"f_header",as_header)
idst_headers.SetItem(li_index,"f_value",as_value)

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Adding Headers...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Return the Row Number
RETURN li_index
end function

public function integer of_setrequestheaders ();///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**  	Object:				w_translate
**		Type: 				Window
**		Function Name:	of_SetRequestHeaders()
**		Brief Description: 	Sets the Request Headers
**  	Parent: 				Window
**		Created by:			Govinda Lopez
**  	Date Created:		Dec/21/2017
**		Modified by:	
**		Date Modified:
**  	Description:			This Function Sets the Request Headers. More info on the RESTFul Web Service (Google Cloud Translation API v2):
**								https://developers.google.com/apis-explorer/?hl=en_US#p/translate/v2/
**
**		Parameters:			(None)
**           					
**		Returns:           	INTEGER
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************
//		Instance Variable Declarations and Initialization
//************************************************************
INTEGER	li_index
INTEGER	li_count
STRING 	ls_header
STRING	ls_value


//************************************************************
//		Logic of the Script
//************************************************************
//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Setting up the Request Headers...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Clear the current Request Headers from the HTTPClient Variable
inv_http.ClearRequestHeaders( )

//	Cycle used to add the Request Headers to the HTTPClient Variable
FOR li_index = 1 TO idst_headers.RowCount( )
	ls_header = idst_headers.getitemstring(li_index, "f_header")
	ls_value = idst_headers.getitemstring(li_index, "f_value")	
		
	//	Set Header into the HTTPClient Variable
	inv_http.SetRequestHeader(ls_header, ls_value)
	
	//	Increment the Counter Variable
	li_count ++
NEXT

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Finished setting up the Request Headers!")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Return the amount of headers added
RETURN li_count
end function

public function integer of_supported_languages ();///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**  	Object:				w_translate
**		Type: 				Window
**		Function Name:	of_Supported_Languages()
**		Brief Description: 	Gets the Supported Languages from the API
**  	Parent: 				Window
**		Created by:			Govinda Lopez
**  	Date Created:		Dec/21/2017
**		Modified by:	
**		Date Modified:
**  	Description:			This Function Gets the Supported Languages from the API and adds them to the DDLBs. More info on the RESTFul Web Service (Google Cloud Translation API v2):
**								https://developers.google.com/apis-explorer/?hl=en_US#p/translate/v2/language.languages.list?model=nmt&target=en&_h=3&
**
**		Parameters:			(None)
**           					
**		Returns:           	INTEGER
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************
//		Instance Variable Declarations and Initialization
//************************************************************
LONG 				ll_ObjectItem
LONG				ll_data,ll_languages,ll_ArrayItem, ll_ChildCount, ll_Root, ll_Index, ll_row, ll_respcode
INTEGER			li_re
STRING			ls_headers
STRING			ls_json
STRING			ls_raw
STRING			ls_name
STRING			ls_language
STRING			ls_key
STRING			ls_type
JSONItemType	ljson_type



//************************************************************
//		Logic of the Script
//************************************************************
//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Calling the RESTFul Web Service for the Supported Languages...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Call Web APIs via the HTTPClient Object using the GET method
li_re = inv_http.SendRequest("GET", "https://translation.googleapis.com/language/translate/v2/languages?model=nmt&target=en&key=" + is_api_key)

//	Get the Response Code from the Web Service
ll_respcode	= inv_http.GetResponseStatusCode()

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Finished calling the RESTFul Web Service for the Supported Languages! Got the Response code: " + STRING(ll_respcode))
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Check if we were able to get the response headers
IF li_re = 1 AND ll_respcode = 200 THEN
	//	Reset the idw_origin_json DW
	idw_origin_json.Reset()
	
	//	Set the DataObject of the idw_origin_json
	idw_origin_json.DataObject	= "d_data_languages"
	
	//	Get the Header of the Response
	ls_headers = inv_http.GetResponseHeaders()
	
	//	Get the body of the response 
	inv_http.GetResponseBody(ls_json)
	
	//	Put the Header, JSON Body and Raw into the MLEs
	tab_origin.tabpage_origin_header.mle_origin_headers.text	= ls_headers
	tab_origin.tabpage_origin_body.mle_origin_body.text			= ls_json
	tab_origin.tabpage_origin_raw.mle_origin_raw.text			= ls_headers + ls_json
	
	//	Load the JSON with the string
	inv_jsonp.LoadString(ls_Json)
	
	//	Get the Root Item
	ll_Root = inv_jsonp.GetRootItem()
		
	//get data object
	ll_data = inv_jsonp.getitemobject(ll_Root,"data")

	//get languages object
	ll_ArrayItem =  inv_jsonp.getitemarray(ll_data,"languages")
	
	//	Get the ArrayItem
	//ll_ArrayItem	= inv_jsonp.GetChildItem(ll_Root, 1)
	
	//	Get the type of JSON Object Type
	//ljson_type	= inv_jsonp.GetItemType(ll_ArrayItem)
	
	//	Check if the ArrayItem is actually an JsonArrayItem!
	//IF ljson_type = JsonArrayItem! THEN
		//	Count the number of Child Items
		ll_ChildCount = inv_jsonp.GetChildCount(ll_ArrayItem)
		
		//	Get items from the array in a loop
		FOR ll_Index = 1 TO ll_ChildCount
			//	Get Each Item
			ll_ObjectItem = inv_jsonp.GetChildItem(ll_ArrayItem, ll_Index)
			
			//	Get the Item Type of each Item
			ljson_type	= inv_jsonp.GetItemType(ll_ObjectItem)
			
			//	Check if the Array Item is a JsonObjectItem!
			IF inv_jsonp.GetItemType(ll_ObjectItem) = JsonObjectItem! THEN
				//	Count the number of languages added
				li_re++
				
				//	Get the Language
				ls_language	= inv_jsonp.GetItemString(ll_ObjectItem, 'language')
				
				//	Get the Name of the Language
	  			ls_Name 		= inv_jsonp.GetItemString(ll_ObjectItem, "name")
				  
				//	Insert the language into the DDLBs
				ddlb_from.InsertItem(ls_language + " - " + ls_name, 1)
				ddlb_to.InsertItem(ls_language + " - " + ls_name, 1)
				
				//	Insert a new row into the DW
				ll_row	= idw_origin_json.InsertRow(0)
				
				//	Set the Items into the DW
				idw_origin_json.SetItem(ll_row, "language", ls_language)
				idw_origin_json.SetItem(ll_row, "name", ls_name)
			END IF
		NEXT
	//END IF
	
	//	Do a Sort on the DDLBs
	ddlb_from.Sorted	= TRUE
	ddlb_to.Sorted		= TRUE
	
	//	Select the Default Languages
	of_Set_Lang_From('en')
	of_Set_Lang_to('es')
END IF

////import data into datawindow
////PB 2017 R2 will add a RestClient object and provide a method in the object for automatically importing data into datawindow
////the code below is for this demo only
////PB2017R2: inv_RestClient.CallWebAPI(dw_1, ls_url_webapi)
//
//this.of_importdata(ls_json)

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Finished calling the RESTFul Web Service for the Supported Languages!")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Return the amount of supported languages
RETURN li_re
end function

public subroutine of_set_lang_from (string as_lang);///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**  	Object:				w_translate
**		Type: 				Window
**		Function Name:	of_Set_Lang_From()
**		Brief Description: 	Sets the Language From
**  	Parent: 				Window
**		Created by:			Govinda Lopez
**  	Date Created:		Dec/21/2017
**		Modified by:	
**		Date Modified:
**  	Description:			This Function changes the Language FROM which to translate from. It also sets upt the Instance variable.
**
**		Parameters:			STRING	as_lang	//	Variable used to pass in the Language to setup FROM
**           					
**		Returns:           	(None)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************
//		Local Variable Declarations and Initialization
//************************************************************
INTEGER	li_item	//	Used to store the Item Index



//************************************************************
//		Logic of the Script
//************************************************************
//	Find the Item and Select it
li_item	= ddlb_from.selectitem(as_lang, 1)

//	Set the Instance Variable
is_lang_from	= as_lang
end subroutine

public subroutine of_set_lang_to (string as_lang);///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**  	Object:				w_translate
**		Type: 				Window
**		Function Name:	of_Set_Lang_To()
**		Brief Description: 	Sets the Language To
**  	Parent: 				Window
**		Created by:			Govinda Lopez
**  	Date Created:		Dec/21/2017
**		Modified by:	
**		Date Modified:
**  	Description:			This Function changes the Language TO the one you want to translate to. It also sets upt the Instance variable.
**
**		Parameters:			STRING	as_lang	//	Variable used to pass in the Language to setup TO
**           					
**		Returns:           	(None)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************
//		Local Variable Declarations and Initialization
//************************************************************
INTEGER	li_item	//	Used to store the Item Index



//************************************************************
//		Logic of the Script
//************************************************************
//	Find the Item and Select it
li_item	= ddlb_to.selectitem(as_lang, 1)

//	Set the Instance Variable
is_lang_to	= as_lang
end subroutine

public function string of_parsejson_lang_detect (string as_json);///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**  	Object:				w_translate
**		Type: 				Window
**		Function Name:	of_ParseJSON_Lang_Detect()
**		Brief Description: 	Processes the JSON for the Language Detection
**  	Parent: 				Window
**		Created by:			Govinda Lopez
**  	Date Created:		Dec/27/2017
**		Modified by:	
**		Date Modified:
**  	Description:			This Function processes the JSON for the Language Detection. More info on the RESTFul Web Service (Google Cloud Translation API v2):
**								https://developers.google.com/apis-explorer/?hl=en_US#p/translate/v2/language.detections.detect?_h=4&resource=%257B%250A++%2522q%2522%253A+%250A++%255B%2522%25C2%25A1Hola+Mundo!%2522%250A++%255D%250A%257D&
**
**		Parameters:			STRING	as_json	//	Variable used to pass in the JSON from the Google API's Language Detection function
**           					
**		Returns:           	STRING	//	The Language Detected
**		
**		NOTES:								//	Parse JSON like the following:
** ln - Level - Object 								
** 1 - Root									//	{
** 2 - 1st Level							//		"data":	{
** 3 - 2nd Level - Array					//						"detections":	[
** 4 - 3rd Level - ChildArray			//												[
** 5 - 4th Level - Items of ChildArray	//													{
** 6 - 5th Level - JSON Object			//														"confidence": 1,
** 7 - 5th Level - JSON Object			//														"isReliable": false,
** 8 - 5th Level - JSON Object			//														"language": "es"
** 9 - 4th Level - Items of ChildArray	//													}
** 10 - 3rd Level - ChildArray			//												]
** 11 - 2nd Level - Array				//											]
** 12 - 1st Level							//					}
** 13 - Root								//	}
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************
//		Variable Declarations and Initialization
//************************************************************
LONG				ll_root										//	Root of the JSON
LONG				ll_data_handle  							//	Handle of "data" Object
LONG				ll_detections_array_handle				//	Handle of "detections" Array
LONG				ll_detections_childarray_handle			//	Child handle of "detections" Array
LONG				ll_detections_childarray_item			//	Parent handle of "detail" Data
LONG				ll_loop_into_childarray					//	Loop counter variable for the ChildArray Items
LONG				ll_loop_json_objects						//	Loop counter variable for the ChildArray Item Objects
LONG				ll_detections_childarray_count			//	Count of ChildArray Objects from the "detections" Array
LONG				ll_detections_childarray_item_count	//	Count of ChildArray Items from the "detections" Array
LONG				ll_row											//	Inserted Row counter
LONG				ll_item										//	Handle of the Item from the ChildArray
DOUBLE			ld_data										//	"Data" JSON Object from the JSON for Language Detection
STRING			ls_value										//	Value of the JSON Object
STRING			ls_return										//	Return variable only used to know if the JSON was loaded successfuly
STRING			ls_key											//	Key of the JSON Object
STRING			ls_lang										//	Detected Language from the JSON
JSONParser		ljs_parser									//	JSON Parser Object
JSONItemType	ljs_type										//	JSON Type Object

//	Create the JSON Object
ljs_parser = CREATE JSONParser



//************************************************************
//		Logic of the Script
//************************************************************
//	Load the JSON String passed as an argument (as_json)
ls_return = ljs_parser.loadstring( as_json)

//	Check whether as_json is a valid json
IF len(trim(ls_return)) > 0 THEN
	//	Notify the User
	MessageBox("Error","Load "+as_json+" failed:"+ls_return)
	
	//	Set the Status of the Program
	il_last_status_row	= dw_status.InsertRow(0)
	dw_status.SetItem(il_last_status_row, "status", "Load "+as_json+" failed:"+ls_return)
	dw_status.SetRow(il_last_status_row)
	dw_status.ScrollToRow(il_last_status_row)
	
	//	Return an Error code
	RETURN "-1"
END IF

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Processing the Language Detection JSON...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Reset the Origin JSON DW
idw_origin_json.Reset()

//	Set the Origin JSON DW's DataObject
idw_origin_json.DataObject = "d_data_original"

//	Get the Root Item of the JSON 																**Line 1 of the Notes above
ll_root									= ljs_parser.GetRootItem()

//	Get the handle of "data" object from the JSON 											**Line 2 of the Notes above
ll_data_handle							= ljs_parser.GetChildItem(ll_root, 1)

//	Get the handle of the "detections" Array from the JSON 								**Line 3 of the Notes above
ll_detections_array_handle			= ljs_parser.GetChildItem(ll_data_handle, 1)

//	Get the handle of the ChildArry of "detections" Array from the JSON				**Line 4 of the Notes above
ll_detections_childarray_handle		= ljs_parser.GetChildItem(ll_detections_array_handle, 1)

//	Get the count of objects in the ChildArray of "detections" Array from the JSON	**Line 5 of the Notes above
ll_detections_childarray_count 		= ljs_parser.GetChildCount(ll_detections_childarray_handle)

//	Cycle used to Loop into each ChildArray Item											**Lines 5-9 of the Notes above
FOR ll_loop_into_childarray = 1 TO ll_detections_childarray_count
	
	//	Each child item is just like one row of a DataWindow
	ll_row									= idw_origin_json.InsertRow(0)
	
	//	Get the handle of the "detections" ChildArray Item detail JSON object			**Lines 5-9 of the Notes above
	ll_detections_childarray_item	= ljs_parser.GetChildItem(ll_detections_childarray_handle, ll_loop_into_childarray)
	
	//	Get the count of the elements inside of the Item from the "detections" ChildArray	**Line 5 of the Notes above
	ll_detections_childarray_item_count = ljs_parser.GetChildCount(ll_detections_childarray_item)
	
	//	Cycle used to get each JSON object from the ChildArray							**Lines 6-8 of the Notes above
	FOR ll_loop_json_objects = 1 TO ll_detections_childarray_item_count
		//	Get the Next JSON Object handle from the ChildArray Item					**Lines 6-8 of the Notes above
		ll_item	= ljs_parser.GetChildItem(ll_detections_childarray_item, ll_loop_json_objects)
		
		//	Get the Next JSON Key from the ChildArray Item								**Lines 6-8 of the Notes above
		ls_key 	= ljs_parser.GetChildKey(ll_detections_childarray_item, ll_loop_json_objects)
		
		//	Get the Next JSON Object type from the ChildArray Item						**Lines 6-8 of the Notes above
		ljs_type = ljs_parser.GetItemType(ll_item)
		
		//	Check what type of JSON Object it is												**Lines 6-8 of the Notes above
		CHOOSE CASE	ljs_type
			CASE jsonstringitem!		//	If the JSON Object is a STRING
				//	Get the value of the JSON Object. Language Detected!				**Line 8 of the Notes above
				ls_lang	= ljs_parser.GetItemString(ll_item)
				
				//	Set the value into the DW
				idw_origin_json.setitem(ll_row, ls_key, ls_lang)
				
			CASE jsonbooleanitem!	//	If the JSON Object is a BOOLEAN
				//	Get the value of the JSON Object and convert it into STRING		**Line 7 of the Notes above
				ls_value	= string(ljs_parser.getitemboolean( ll_item))
				
				//	Set the value into the DW
				idw_origin_json.setitem(ll_row, ls_key, ls_value)
				
			CASE jsonnumberitem!	//	If the JSON Object is a NUMBER				**Line 6 of the Notes above
				//	Get the value of the JSON Object
				ld_data	= ljs_parser.getitemnumber(ll_item)
				
				//	Set the value into the DW
				idw_origin_json.setitem(ll_row, ls_key, ld_data)
			
			CASE ELSE		//	Just in case another value comes up
				//	Continue to the next cycle
				CONTINUE
		END CHOOSE
	NEXT
NEXT

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Language Detection JSON Processed!")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Return the value of the Detected Language
RETURN ls_lang
end function

public function string of_parsejson_translate (string as_json);///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**  	Object:				w_translate
**		Type: 				Window
**		Function Name:	of_ParseJSON_Translate()
**		Brief Description: 	Processes the JSON for the Translation
**  	Parent: 				Window
**		Created by:			Govinda Lopez
**  	Date Created:		Dec/27/2017
**		Modified by:	
**		Date Modified:
**  	Description:			This Function processes the JSON for the Translation. More info on the RESTFul Web Service (Google Cloud Translation API v2): 
**								https://developers.google.com/apis-explorer/?hl=en_US#p/translate/v2/language.translations.translate?_h=2&resource=%257B%250A++%2522format%2522%253A+%2522text%2522%252C%250A++%2522model%2522%253A+%2522nmt%2522%252C%250A++%2522q%2522%253A+%250A++%255B%2522%25C2%25A1Hola+mundo!%2522%250A++%255D%252C%250A++%2522source%2522%253A+%2522es%2522%252C%250A++%2522target%2522%253A+%2522en%2522%250A%257D&
**
**		Parameters:			STRING	as_json	//	Variable used to pass in the JSON from the Google API's Translate function
**           					
**		Returns:           	STRING	//	The Translated Text
**		
**		NOTES:								//	Parse JSON like the following:
** ln - Level - Object 								
** 1 - Root									//	{
** 2 - 1st Level							//		"data":	{
** 3 - 2nd Level - Array					//						"translations":	[
** 4 - 4th Level - Items of Array		//													{
** 5 - 5th Level - JSON Object			//														"translatedText": "Hello world!",
** 6 - 5th Level - JSON Object			//														"model": "nmt"
** 7 - 4th Level - Items of Array		//													}
** 8 - 2nd Level - Array					//											]
** 9 - 1st Level							//					}
** 10 - Root								//	}
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************
//		Variable Declarations and Initialization
//************************************************************
LONG				ll_root									//	Root of the JSON
LONG				ll_data_handle  						//	Handle of "data" Object
LONG				ll_translations_array_handle			//	Handle of "translations" array
LONG 				ll_translations_array_item			//	Handle of "translations" array item
LONG 				ll_loop_into_array						//	Loop counter variable for the Array Items
LONG				ll_loop_json_objects					//	Loop counter variable for the Array Item Objects
LONG				ll_translations_array_count			//	Count of the "translations" Array objects
LONG				ll_translations_array_item_count	//	Count of the "translations" Array Item Objects
LONG				ll_row										//	Inserted Row counter
LONG				ll_item									//	Handle of the Item from the Array
DOUBLE 			ld_data									//	"Data" JSON Object from the JSON for the Translation
STRING 			ls_value									//	Value of the JSON Object
STRING			ls_return									//	Return variable only used to know if the JSON was loaded successfuly
STRING			ls_key										//	Key of the JSON Object
JSONParser 		ljs_Parser								//	JSON Parser Object
JSONItemType	ljs_Type									//	JSON Type Object

//	Create the JSON Object
ljs_Parser = CREATE JSONParser



//************************************************************
//		Logic of the Script
//************************************************************
//	Load the JSON String passed as an argument (as_json)
ls_return = ljs_Parser.LoadString( as_json)

//	Check whether as_json is a valid json
IF LEN(TRIM(ls_return)) > 0 THEN
	//	Notify the User
	MessageBox("Error","Load "+as_json+" failed:"+ls_return)
	
	//	Set the Status of the Program
	il_last_status_row	= dw_status.InsertRow(0)
	dw_status.SetItem(il_last_status_row, "status", "Load "+as_json+" failed:"+ls_return)
	dw_status.SetRow(il_last_status_row)
	dw_status.ScrollToRow(il_last_status_row)
	
	//	Return an Error code
	RETURN "-1"
END IF

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Processing the Translation JSON...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Reset the Translate JSON DW
idw_trans_json.Reset()

//	Set the Translate JSON DW's DataObject
idw_trans_json.DataObject = "d_data_translate"

//	Get the Root Item of the JSON 																**Line 1 of the Notes above
ll_root 							= ljs_Parser.GetRootItem()

//	Get the handle of "data" object from the JSON 											**Line 2 of the Notes above
ll_data_handle 					= ljs_Parser.GetChildItem(ll_root, 1)

//	Get the handle of "translations" array														**Line 3 of the Notes above
ll_translations_array_handle	= ljs_Parser.GetChildItem(ll_data_handle, 1)

//	Get the count of objects in the "translations" Array from the JSON					**Line 4 of the Notes above
ll_translations_array_count							= ljs_Parser.GetChildCount(ll_translations_array_handle)

//	Cycle used to Loop into each Array Item													**Lines 4-7 of the Notes above
FOR ll_loop_into_array = 1 TO ll_translations_array_count
	//	Each child item is just like one row of a DataWindow
	ll_row										= idw_trans_json.InsertRow(0)
	
	//	Get the handle of the "translations" Array Item detail JSON object				**Lines 5-6 of the Notes above
	ll_translations_array_item			= ljs_Parser.GetChildItem(ll_translations_array_handle, ll_loop_into_array)
	
	//	Get the count of the elements inside of the Item from the "translations" Array	**Line 4 of the Notes above
	ll_translations_array_item_count	= ljs_Parser.GetChildCount(ll_translations_array_item)
	
	//	Cycle used to get each JSON object from the Array								**Lines 5-6 of the Notes above
	FOR ll_loop_json_objects = 1 TO ll_translations_array_item_count
		//	Get the Next JSON Object handle from the Array Item							**Lines 5-6 of the Notes above
		ll_item	= ljs_Parser.GetChildItem(ll_translations_array_item, ll_loop_json_objects)
		
		//	Get the Next JSON Key from the ChildArray Item								**Lines 5-6 of the Notes above
		ls_key		= ljs_Parser.GetChildKey(ll_translations_array_item, ll_loop_json_objects)
		
		//	Get the Next JSON Object type from the ChildArray Item						**Lines 5-6 of the Notes above
		ljs_Type	= ljs_Parser.GetItemType(ll_item)
		
		//	Check what type of JSON Object it is												**Lines 5-6 of the Notes above
		CHOOSE CASE ljs_Type
			CASE JSONStringItem!	//	If the JSON Object is a STRING
				//	Get the value of the JSON Object. translatedText!						**Line 5 of the Notes above
				ls_value = ljs_Parser.GetItemString(ll_item)
				
				//	Set the value into the DW
				idw_trans_json.SetItem(ll_row, ls_key, ls_value)
				
				//	Check if it's the translatedText
				IF	ls_key	= "translatedText" THEN
					//	Set the value of the RETURN variable
					ls_return	= ls_value
				END IF
			CASE ELSE				//	Just in case another value comes up
				//	Continue to the next cycle
				CONTINUE	
		END CHOOSE
	NEXT
NEXT

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Finished Processing the Translation JSON!")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Return the Translated Text
RETURN ls_return
end function

on w_translate.create
this.dw_status=create dw_status
this.p_logo=create p_logo
this.pb_translate=create pb_translate
this.cbx_autodetect=create cbx_autodetect
this.ddlb_to=create ddlb_to
this.st_to=create st_to
this.ddlb_from=create ddlb_from
this.st_from=create st_from
this.tab_translation=create tab_translation
this.tab_origin=create tab_origin
this.gb_origin=create gb_origin
this.gb_translation=create gb_translation
this.gb_lang=create gb_lang
this.p_source=create p_source
this.p_translate=create p_translate
this.Control[]={this.dw_status,&
this.p_logo,&
this.pb_translate,&
this.cbx_autodetect,&
this.ddlb_to,&
this.st_to,&
this.ddlb_from,&
this.st_from,&
this.tab_translation,&
this.tab_origin,&
this.gb_origin,&
this.gb_translation,&
this.gb_lang,&
this.p_source,&
this.p_translate}
end on

on w_translate.destroy
destroy(this.dw_status)
destroy(this.p_logo)
destroy(this.pb_translate)
destroy(this.cbx_autodetect)
destroy(this.ddlb_to)
destroy(this.st_to)
destroy(this.ddlb_from)
destroy(this.st_from)
destroy(this.tab_translation)
destroy(this.tab_origin)
destroy(this.gb_origin)
destroy(this.gb_translation)
destroy(this.gb_lang)
destroy(this.p_source)
destroy(this.p_translate)
end on

event open;///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**  	Object:				w_translate
**		Type: 				Window
**		Event Name:		Open()
**		Brief Description: 	Open event of the Window
**  	Parent: 				Window
**		Created by:			Govinda Lopez
**  	Date Created:		Dec/21/2017
**		Modified by:	
**		Date Modified:
**  	Description:			Open event of the Window. Creates the Object Variables for the NEW Features of PowerBuilder 2017 R2 and Initializes the Headers and Gets the 
**								Supported languages. More info on the RESTFul Web Service (Google Cloud Translation API v2):
**								https://developers.google.com/apis-explorer/?hl=en_US#p/translate/v2/
**
**		Parameters:			(None)
**           					
**		Returns:           	(None)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************
//		Instance Variable Declarations and Initialization
//************************************************************
//	Create all the Instance variable objects
inv_http				= CREATE HTTPCLIENT
inv_jsonp			= CREATE JSONPARSER
inv_jsong			= CREATE JSONGenerator
inv_restc				= CREATE RESTClient
idst_headers		= CREATE DATASTORE
idw_origin_json	= CREATE DATAWINDOW
idw_trans_json		= CREATE DATAWINDOW



//************************************************************
//		Logic of the Script
//************************************************************
//	Set the Pointer
SetPointer(HourGlass!)

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Initializing...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Set the DataWindow Origin JSON Instance Variable
idw_origin_json	= tab_origin.tabpage_origin_bodydw.dw_json_body

//	Set the DataWindow's DataObject for the Origin JSON Instance Variable
idw_origin_json.DataObject	= "d_data_original"

//	Set the datastore's DataObject
idst_headers.dataobject	= "d_headers"

//	Set the DataWindow Translate JSON Instance Variable
idw_trans_json		= tab_translation.tabpage_trans_bodydw.dw_trans_json_body

//	Set the DataWindow's DataObject for the Translate JSON Instance Variable
idw_trans_json.DataObject	= "d_data_translate"

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Getting Google's API Key from the Configuration Tab...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Get the API Key from the Configuration SLE in the tab_origin
is_api_key	= TRIM(tab_origin.tabpage_origin_api_key.sle_api_key.text)

//	Check if there is an API Key
IF ISNULL(is_api_key) OR is_api_key = "" THEN
	//	Notify the user
	MessageBox("Warning!", "No Google API Key has been captured. This info is required in order to consume the Web Service.", StopSign!)
	
	//	Select the SLE for the API Key
	tab_origin.SelectTab(5)
	
	//	Set the focus on the SLE
	tab_origin.tabpage_origin_api_key.sle_api_key.SetFocus()
END IF

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Initializing Request Headers to get the Supported Languages List...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Set the Initial Headers to get the supported language list
of_initial_requestheaders()

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Getting the Supported Languages List from Google's API RESTFul Web Service...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Get the Supported Languages via the Web API
of_supported_languages()

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Updated the List of the Supported Languages!")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Set the focus on the MLE
tab_origin.tabpage_origin_text.mle_origin_text.SetFocus()

//	Set the Pointer
SetPointer(Arrow!)
end event

event close;//	Destroy the Objects from Memory
destroy	inv_http
destroy	inv_jsonp
destroy	inv_jsong
destroy	inv_restc
end event

type dw_status from datawindow within w_translate
integer x = 14
integer y = 2404
integer width = 5810
integer height = 296
integer taborder = 50
string title = "none"
string dataobject = "d_status"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;SelectRow(0, FALSE)
SelectRow(CurrentRow, TRUE)
end event

type p_logo from picture within w_translate
integer x = 2615
integer y = 156
integer width = 585
integer height = 512
boolean originalsize = true
string picturename = "images\logo2.png"
boolean focusrectangle = false
end type

type pb_translate from picturebutton within w_translate
integer x = 2587
integer y = 1432
integer width = 631
integer height = 152
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string picturename = "images\translate.png"
end type

event clicked;///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**  	Object:				cb_translate
**		Type: 				button
**		Event Name:		Clicked()
**		Brief Description: 	Clicked Event of the Button
**  	Parent: 				Window
**		Created by:			Govinda Lopez
**  	Date Created:		Dec/21/2017
**		Modified by:	
**		Date Modified:
**  	Description:			This Event Translates the Text from the MLE Origin Text. More info on the RESTFul Web Service (Google Cloud Translation API v2): 
**								https://developers.google.com/apis-explorer/?hl=en_US#p/translate/v2/language.translations.translate?_h=2&resource=%257B%250A++%2522format%2522%253A+%2522text%2522%252C%250A++%2522model%2522%253A+%2522nmt%2522%252C%250A++%2522q%2522%253A+%250A++%255B%2522%25C2%25A1Hola+mundo!%2522%250A++%255D%252C%250A++%2522source%2522%253A+%2522es%2522%252C%250A++%2522target%2522%253A+%2522en%2522%250A%257D&
**
**		Parameters:			(None)
**           					
**		Returns:           	(None)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************
//		Instance Variable Declarations and Initialization
//************************************************************
STRING	ls_text_from		//	The Text FROM
STRING	ls_text_to		//	The Translated Text
STRING	ls_json			//	The JSON String
STRING	ls_headers		//	The Headers
STRING	ls_key				//	Key Value of the JSON object
INTEGER	li_re				//	Return Code
INTEGER	li_respcode		//	Response Code
LONG		ll_Root			//	Root Object Variable for the JSON Generator
STRING	ls_webapiurl //	The URL of Web API


//************************************************************
//		Logic of the Script
//************************************************************
//	Set the Pointer
SetPointer(HourGlass!)

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Translating...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Get the Text FROM
ls_text_from	= TRIM(tab_origin.tabpage_origin_text.mle_origin_text.Text)

//	Check if there is something entered in the Origin Text field
IF LEN(ls_text_from) <= 0 THEN
	//	Notify the user
	MessageBox("Warning!", "Nothing captured in the Source Text.", Information!)
	
	//	Set the Status of the Program
	il_last_status_row	= dw_status.InsertRow(0)
	dw_status.SetItem(il_last_status_row, "status", "Nothing captured in the Source Text.")
	dw_status.SetRow(il_last_status_row)
	dw_status.ScrollToRow(il_last_status_row)
	
	//	Just Return
	RETURN
END IF

//	Check if the Translate FROM and TO languages are the same
IF	is_lang_from = is_lang_to THEN
	//	Just copy the text
	tab_translation.tabpage_trans_text.mle_trans_text.text	= ls_text_from
	
	//	Set the Status of the Program
	il_last_status_row	= dw_status.InsertRow(0)
	dw_status.SetItem(il_last_status_row, "status", "Finished Translating! That was easy! Same language.")
	dw_status.SetRow(il_last_status_row)
	dw_status.ScrollToRow(il_last_status_row)
	
	//	Return
	RETURN
END IF

//	Reset the DataStore
idst_headers.Reset()

//	Set the datastore's DataObject
idst_headers.DataObject	= "d_headers"

//	Add the Request Headers
of_AddRequestHeader('format', "text")
of_AddRequestHeader('model', "nmt")
of_AddRequestHeader('q', ls_text_from)
of_AddRequestHeader('source', is_lang_from)
of_AddRequestHeader('target', is_lang_to)

//	Set the Request Headers
of_SetRequestHeaders()

//	Create the JSON object
ll_Root	= inv_jsong.CreateJsonObject()

//	Add the Item 'format' along with it's value
inv_jsong.AddItemString(ll_Root, "format", "text")

//	Add the Item 'model' along with it's value
inv_jsong.AddItemString(ll_Root, "model", "nmt")

//	Add the Item 'q' along with it's value
inv_jsong.AddItemString(ll_Root, "q", ls_text_from)

//	Add the Item 'source' along with it's value
inv_jsong.AddItemString(ll_Root, "source", is_lang_from)

//	Add the Item 'target' along with it's value
inv_jsong.AddItemString(ll_Root, "target", is_lang_to)

//	Gets the String of the JSON
ls_json	= inv_jsong.GetJsonString()

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Calling the RESTFul Web Service for the Translation...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Call Web APIs via the HTTPClient Object using the POST method
ls_webapiurl = "https://translation.googleapis.com/language/translate/v2?key=" + is_api_key
li_re = inv_http.SendRequest("POST", ls_webapiurl, ls_json)

//	Get the Response Status Code
li_respcode	= inv_http.GetResponseStatusCode()

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Finished calling the RESTFul Web Service for the Translation! Got the Response Code: " + STRING(li_respcode))
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Check if we were able to get the response headers
IF li_re = 1 AND li_respcode = 200 THEN
	//	Get the Header of the Response
	ls_headers = inv_http.GetResponseHeaders()
	
	//	Get the body of the response 
	inv_http.GetResponseBody(ls_json)
	
	//	Set the Status of the Program
	il_last_status_row	= dw_status.InsertRow(0)
	dw_status.SetItem(il_last_status_row, "status", "Filling the Translations Tabs with data...")
	dw_status.SetRow(il_last_status_row)
	dw_status.ScrollToRow(il_last_status_row)
	
	//	Put the Header, Body and Raw data into the MLEs
	tab_translation.tabpage_trans_headers.mle_trans_headers.text	= ls_headers
	tab_translation.tabpage_trans_json.mle_trans_body.text				= ls_json
	tab_translation.tabpage_trans_raw.mle_trans_raw.text				= ls_headers + ls_json
	
	//	Set the Status of the Program
	il_last_status_row	= dw_status.InsertRow(0)
	dw_status.SetItem(il_last_status_row, "status", "Finished filling the Translations Tabs with data...")
	dw_status.SetRow(il_last_status_row)
	dw_status.ScrollToRow(il_last_status_row)
	
	//	Load the JSON Response into the JSONParser
	inv_jsonp.LoadString(ls_json)

	//	Call the function to get the Translated text out of the Response JSON
	ls_text_to	= of_ParseJSON_Translate(ls_json)
	
	//	Put the Translated Text into the MLE
	tab_translation.tabpage_trans_text.mle_trans_text.text	= ls_text_to
	
	//	Set the Status of the Program
	il_last_status_row	= dw_status.InsertRow(0)
	dw_status.SetItem(il_last_status_row, "status", "Finished Translating! Oh boy!")
	dw_status.SetRow(il_last_status_row)
	dw_status.ScrollToRow(il_last_status_row)
END IF

//	Set the Pointer
SetPointer(Arrow!)
end event

type cbx_autodetect from checkbox within w_translate
integer x = 2633
integer y = 1308
integer width = 549
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 553648127
string text = "Auto Detect"
boolean checked = true
end type

event clicked;///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**  	Object:				cbx_autodetect
**		Type: 				CheckBox
**		Event Name:		Clicked()
**		Brief Description: 	Clicked Event
**  	Parent: 				Window
**		Created by:			Govinda Lopez
**  	Date Created:		Dec/21/2017
**		Modified by:	
**		Date Modified:
**  	Description:			This Event Detects the Language FROM which to Translate
**
**		Parameters:			(None)
**           					
**		Returns:           	(None)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************
//		Instance Variable Declarations and Initialization
//************************************************************
STRING	ls_text		//	The Text FROM
STRING	ls_json		//	The JSON String
STRING	ls_headers	//	The Headers
STRING	ls_lang		//	The Detected Language
STRING	ls_key			//	Key Value of the JSON object
INTEGER	li_re			//	Return Code
INTEGER	li_respcode	//	Response Code
LONG		ll_Root		//	Root Object Variable for the JSON Generator


//************************************************************
//		Logic of the Script
//************************************************************
//	Verify if this Control Is Checked
IF Checked	= FALSE THEN
	//	Just Return
	RETURN
END IF

//	Set the Pointer
SetPointer(HourGlass!)

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Detecting Language...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Get the Text FROM
ls_text	= TRIM(tab_origin.tabpage_origin_text.mle_origin_text.Text)

//	Check if there is something entered in the Origin Text field
IF LEN(ls_text) <= 0 THEN
	//	Notify the user
	MessageBox("Warning!", "Nothing captured in the Original Text.", Information!)
	
	//	Uncheck this controls
	Checked	= FALSE
	
	//	Just Return
	RETURN
END IF

//	Reset the DataStore
idst_headers.Reset()

//	Set the datastore's DataObject
idst_headers.DataObject	= "d_headers"

//	Add the Request Headers
of_AddRequestHeader('q', ls_text)

//	Set the Request Headers
of_SetRequestHeaders()

//	Create the JSON object
ll_Root	= inv_jsong.CreateJsonObject()

//	Add the Item 'q' along with it's value
inv_jsong.AddItemString(ll_Root, "q", ls_text)

//	Gets the String of the JSON
ls_json	= inv_jsong.GetJsonString()

//	Call Web APIs via the HTTPClient Object using the POST method
li_re = inv_http.SendRequest("POST", "https://translation.googleapis.com/language/translate/v2/detect?key=" + is_api_key, ls_json)

//	Get the Response Status Code
li_respcode	= inv_http.GetResponseStatusCode()

//	Check if we were able to get the response headers
IF li_re = 1 AND li_respcode = 200 THEN
	//	Get the Header of the Response
	ls_headers = inv_http.GetResponseHeaders()
	
	//	Get the body of the response 
	inv_http.GetResponseBody(ls_json)
	
	//	Set the Status of the Program
	il_last_status_row	= dw_status.InsertRow(0)
	dw_status.SetItem(il_last_status_row, "status", "Filling the Source Tabs with data...")
	dw_status.SetRow(il_last_status_row)
	dw_status.ScrollToRow(il_last_status_row)
	
	//	Put the Header, JSON Body and Raw into the MLEs
	tab_origin.tabpage_origin_header.mle_origin_headers.text	= ls_headers
	tab_origin.tabpage_origin_body.mle_origin_body.text			= ls_json
	tab_origin.tabpage_origin_raw.mle_origin_raw.text			= ls_headers + ls_json
	
	//	Set the Status of the Program
	il_last_status_row	= dw_status.InsertRow(0)
	dw_status.SetItem(il_last_status_row, "status", "Finished filling the Source Tabs with data!")
	dw_status.SetRow(il_last_status_row)
	dw_status.ScrollToRow(il_last_status_row)
	
	//	Load the JSON Response into the JSONParser
	inv_jsonp.LoadString(ls_json)

	//	Call the function to get Detected Language out of the Response JSON
	ls_lang	= of_parsejson_lang_detect(ls_json)
	
	//	Set the FROM Language
	of_Set_lang_from(ls_lang)
END IF

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Language Detected!: '" + ls_lang + "'")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Set the Pointer
SetPointer(Arrow!)
end event

type ddlb_to from dropdownlistbox within w_translate
integer x = 2615
integer y = 1168
integer width = 585
integer height = 468
integer taborder = 50
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;//	Set the Language Instance variable's value
is_lang_to	= MID(Text(index), 1, 2)

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Changed the Translation Language...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)
end event

type st_to from statictext within w_translate
integer x = 2702
integer y = 1084
integer width = 402
integer height = 64
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 553648127
string text = "TO"
alignment alignment = center!
boolean focusrectangle = false
end type

type ddlb_from from dropdownlistbox within w_translate
integer x = 2615
integer y = 924
integer width = 585
integer height = 468
integer taborder = 40
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;//	Set the Language Instance variable's value
is_lang_from	= MID(Text(index), 1, 2)

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Changed the Source Language...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)
end event

type st_from from statictext within w_translate
integer x = 2702
integer y = 840
integer width = 402
integer height = 64
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 553648127
string text = "FROM"
alignment alignment = center!
boolean focusrectangle = false
end type

type tab_translation from tab within w_translate
integer x = 3291
integer y = 168
integer width = 2459
integer height = 2216
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 16777215
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_trans_text tabpage_trans_text
tabpage_trans_headers tabpage_trans_headers
tabpage_trans_json tabpage_trans_json
tabpage_trans_bodydw tabpage_trans_bodydw
tabpage_trans_raw tabpage_trans_raw
end type

on tab_translation.create
this.tabpage_trans_text=create tabpage_trans_text
this.tabpage_trans_headers=create tabpage_trans_headers
this.tabpage_trans_json=create tabpage_trans_json
this.tabpage_trans_bodydw=create tabpage_trans_bodydw
this.tabpage_trans_raw=create tabpage_trans_raw
this.Control[]={this.tabpage_trans_text,&
this.tabpage_trans_headers,&
this.tabpage_trans_json,&
this.tabpage_trans_bodydw,&
this.tabpage_trans_raw}
end on

on tab_translation.destroy
destroy(this.tabpage_trans_text)
destroy(this.tabpage_trans_headers)
destroy(this.tabpage_trans_json)
destroy(this.tabpage_trans_bodydw)
destroy(this.tabpage_trans_raw)
end on

type tabpage_trans_text from userobject within tab_translation
integer x = 18
integer y = 112
integer width = 2423
integer height = 2088
long backcolor = 16777215
string text = "Text"
long tabtextcolor = 33554432
long tabbackcolor = 16777215
long picturemaskcolor = 16777215
mle_trans_text mle_trans_text
end type

on tabpage_trans_text.create
this.mle_trans_text=create mle_trans_text
this.Control[]={this.mle_trans_text}
end on

on tabpage_trans_text.destroy
destroy(this.mle_trans_text)
end on

type mle_trans_text from multilineedit within tabpage_trans_text
integer y = 20
integer width = 2409
integer height = 2056
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_trans_headers from userobject within tab_translation
integer x = 18
integer y = 112
integer width = 2423
integer height = 2088
long backcolor = 16777215
string text = "Headers"
long tabtextcolor = 33554432
long tabbackcolor = 16777215
long picturemaskcolor = 16777215
mle_trans_headers mle_trans_headers
end type

on tabpage_trans_headers.create
this.mle_trans_headers=create mle_trans_headers
this.Control[]={this.mle_trans_headers}
end on

on tabpage_trans_headers.destroy
destroy(this.mle_trans_headers)
end on

type mle_trans_headers from multilineedit within tabpage_trans_headers
integer y = 20
integer width = 2409
integer height = 2056
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_trans_json from userobject within tab_translation
integer x = 18
integer y = 112
integer width = 2423
integer height = 2088
long backcolor = 16777215
string text = "JSON Body"
long tabtextcolor = 33554432
long tabbackcolor = 16777215
long picturemaskcolor = 16777215
mle_trans_body mle_trans_body
end type

on tabpage_trans_json.create
this.mle_trans_body=create mle_trans_body
this.Control[]={this.mle_trans_body}
end on

on tabpage_trans_json.destroy
destroy(this.mle_trans_body)
end on

type mle_trans_body from multilineedit within tabpage_trans_json
integer y = 20
integer width = 2409
integer height = 2056
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_trans_bodydw from userobject within tab_translation
integer x = 18
integer y = 112
integer width = 2423
integer height = 2088
long backcolor = 16777215
string text = "JSON Body DW"
long tabtextcolor = 33554432
long tabbackcolor = 16777215
long picturemaskcolor = 536870912
dw_trans_json_body dw_trans_json_body
end type

on tabpage_trans_bodydw.create
this.dw_trans_json_body=create dw_trans_json_body
this.Control[]={this.dw_trans_json_body}
end on

on tabpage_trans_bodydw.destroy
destroy(this.dw_trans_json_body)
end on

type dw_trans_json_body from datawindow within tabpage_trans_bodydw
integer y = 20
integer width = 2409
integer height = 2056
integer taborder = 30
string title = "none"
string dataobject = "d_data_translate"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_trans_raw from userobject within tab_translation
integer x = 18
integer y = 112
integer width = 2423
integer height = 2088
long backcolor = 16777215
string text = "Raw"
long tabtextcolor = 33554432
long tabbackcolor = 16777215
long picturemaskcolor = 16777215
mle_trans_raw mle_trans_raw
end type

on tabpage_trans_raw.create
this.mle_trans_raw=create mle_trans_raw
this.Control[]={this.mle_trans_raw}
end on

on tabpage_trans_raw.destroy
destroy(this.mle_trans_raw)
end on

type mle_trans_raw from multilineedit within tabpage_trans_raw
integer y = 20
integer width = 2409
integer height = 2056
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type tab_origin from tab within w_translate
integer x = 59
integer y = 168
integer width = 2459
integer height = 2216
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 16777215
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_origin_text tabpage_origin_text
tabpage_origin_header tabpage_origin_header
tabpage_origin_body tabpage_origin_body
tabpage_origin_bodydw tabpage_origin_bodydw
tabpage_origin_raw tabpage_origin_raw
tabpage_origin_api_key tabpage_origin_api_key
end type

on tab_origin.create
this.tabpage_origin_text=create tabpage_origin_text
this.tabpage_origin_header=create tabpage_origin_header
this.tabpage_origin_body=create tabpage_origin_body
this.tabpage_origin_bodydw=create tabpage_origin_bodydw
this.tabpage_origin_raw=create tabpage_origin_raw
this.tabpage_origin_api_key=create tabpage_origin_api_key
this.Control[]={this.tabpage_origin_text,&
this.tabpage_origin_header,&
this.tabpage_origin_body,&
this.tabpage_origin_bodydw,&
this.tabpage_origin_raw,&
this.tabpage_origin_api_key}
end on

on tab_origin.destroy
destroy(this.tabpage_origin_text)
destroy(this.tabpage_origin_header)
destroy(this.tabpage_origin_body)
destroy(this.tabpage_origin_bodydw)
destroy(this.tabpage_origin_raw)
destroy(this.tabpage_origin_api_key)
end on

type tabpage_origin_text from userobject within tab_origin
integer x = 18
integer y = 112
integer width = 2423
integer height = 2088
long backcolor = 16777215
string text = "Text"
long tabtextcolor = 33554432
long tabbackcolor = 16777215
long picturemaskcolor = 16777215
string powertiptext = "Original Text to Translate"
mle_origin_text mle_origin_text
end type

on tabpage_origin_text.create
this.mle_origin_text=create mle_origin_text
this.Control[]={this.mle_origin_text}
end on

on tabpage_origin_text.destroy
destroy(this.mle_origin_text)
end on

type mle_origin_text from multilineedit within tabpage_origin_text
integer y = 20
integer width = 2409
integer height = 2056
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

event modified;///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**  	Object:				mle_origin_text
**		Type: 				MultiLineEdit
**		Event Name:		Modified()
**		Brief Description: 	Modified Event
**  	Parent: 				tabpage
**		Created by:			Govinda Lopez
**  	Date Created:		Dec/22/2017
**		Modified by:	
**		Date Modified:
**  	Description:			This Event checks if Language detection is selected. Then it Detects the Language FROM which to Translate
**
**		Parameters:			(None)
**           					
**		Returns:           	(None)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//************************************************************
//		Instance Variable Declarations and Initialization
//************************************************************
STRING	ls_text		//	The Text FROM
STRING	ls_json		//	The JSON String
STRING	ls_headers	//	The Headers
STRING	ls_lang		//	The Detected Language
STRING	ls_key			//	Key Value of the JSON object
INTEGER	li_re			//	Return Code
INTEGER	li_respcode	//	Response Code
LONG		ll_Root		//	Root Object Variable for the JSON Generator


//************************************************************
//		Logic of the Script
//************************************************************
//	Verify if this Control Is Checked
IF cbx_autodetect.Checked	= FALSE THEN
	//	Just Return
	RETURN
END IF

//	Set the Pointer
SetPointer(HourGlass!)

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Detecting Language...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Get the Text FROM
ls_text	= TRIM(THIS.Text)

//	Reset the DataStore
idst_headers.Reset()

//	Set the datastore's DataObject
idst_headers.DataObject	= "d_headers"

//	Add the Request Headers
of_AddRequestHeader('q', ls_text)

//	Set the Request Headers
of_SetRequestHeaders()

//	Create the JSON object
ll_Root	= inv_jsong.CreateJsonObject()

//	Add the Item 'q' along with it's value
inv_jsong.AddItemString(ll_Root, "q", ls_text)

//	Gets the String of the JSON
ls_json	= inv_jsong.GetJsonString()

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Calling the RESTFul Web Service for the Language Detection...")
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Call Web APIs via the HTTPClient Object using the POST method
li_re = inv_http.SendRequest("POST", "https://translation.googleapis.com/language/translate/v2/detect?key=" + is_api_key, ls_json)

//	Get the Response Status Code
li_respcode	= inv_http.GetResponseStatusCode()

//	Set the Status of the Program
il_last_status_row	= dw_status.InsertRow(0)
dw_status.SetItem(il_last_status_row, "status", "Finished calling the RESTFul Web Service for the Language Detection! Got response code: " + STRING(li_respcode))
dw_status.SetRow(il_last_status_row)
dw_status.ScrollToRow(il_last_status_row)

//	Check if we were able to get the response headers
IF li_re = 1 AND li_respcode = 200 THEN
	//	Get the Header of the Response
	ls_headers = inv_http.GetResponseHeaders()
	
	//	Get the body of the response 
	inv_http.GetResponseBody(ls_json)
	
	//	Set the Status of the Program
	il_last_status_row	= dw_status.InsertRow(0)
	dw_status.SetItem(il_last_status_row, "status", "Filling the Source Tabs with data...")
	dw_status.SetRow(il_last_status_row)
	dw_status.ScrollToRow(il_last_status_row)
	
	//	Put the Header, JSON Body and Raw into the MLEs
	tab_origin.tabpage_origin_header.mle_origin_headers.text	= ls_headers
	tab_origin.tabpage_origin_body.mle_origin_body.text			= ls_json
	tab_origin.tabpage_origin_raw.mle_origin_raw.text			= ls_headers + ls_json
	
	//	Set the Status of the Program
	il_last_status_row	= dw_status.InsertRow(0)
	dw_status.SetItem(il_last_status_row, "status", "Finished filling the Source Tabs with data!")
	dw_status.SetRow(il_last_status_row)
	dw_status.ScrollToRow(il_last_status_row)
	
	//	Load the JSON Response into the JSONParser
	inv_jsonp.LoadString(ls_json)
	
	//	Get the Detected Language from the Response JSON by calling the Function
	ls_lang	= of_parsejson_lang_detect(ls_json)
	
	//	Set the FROM Language
	of_Set_lang_from(ls_lang)
	
	//	Set the Status of the Program
	il_last_status_row	= dw_status.InsertRow(0)
	dw_status.SetItem(il_last_status_row, "status", "Language Detected!: '" + ls_lang + "'")
	dw_status.SetRow(il_last_status_row)
	dw_status.ScrollToRow(il_last_status_row)
END IF

//	Set the Pointer
SetPointer(Arrow!)
end event

type tabpage_origin_header from userobject within tab_origin
integer x = 18
integer y = 112
integer width = 2423
integer height = 2088
long backcolor = 16777215
string text = "Headers"
long tabtextcolor = 33554432
long tabbackcolor = 16777215
long picturemaskcolor = 16777215
mle_origin_headers mle_origin_headers
end type

on tabpage_origin_header.create
this.mle_origin_headers=create mle_origin_headers
this.Control[]={this.mle_origin_headers}
end on

on tabpage_origin_header.destroy
destroy(this.mle_origin_headers)
end on

type mle_origin_headers from multilineedit within tabpage_origin_header
integer y = 20
integer width = 2409
integer height = 2056
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_origin_body from userobject within tab_origin
integer x = 18
integer y = 112
integer width = 2423
integer height = 2088
long backcolor = 16777215
string text = "JSON Body"
long tabtextcolor = 33554432
long tabbackcolor = 16777215
long picturemaskcolor = 16777215
mle_origin_body mle_origin_body
end type

on tabpage_origin_body.create
this.mle_origin_body=create mle_origin_body
this.Control[]={this.mle_origin_body}
end on

on tabpage_origin_body.destroy
destroy(this.mle_origin_body)
end on

type mle_origin_body from multilineedit within tabpage_origin_body
integer y = 20
integer width = 2409
integer height = 2056
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_origin_bodydw from userobject within tab_origin
integer x = 18
integer y = 112
integer width = 2423
integer height = 2088
long backcolor = 16777215
string text = "JSON Body DW"
long tabtextcolor = 33554432
long tabbackcolor = 16777215
long picturemaskcolor = 536870912
dw_json_body dw_json_body
end type

on tabpage_origin_bodydw.create
this.dw_json_body=create dw_json_body
this.Control[]={this.dw_json_body}
end on

on tabpage_origin_bodydw.destroy
destroy(this.dw_json_body)
end on

type dw_json_body from datawindow within tabpage_origin_bodydw
integer y = 20
integer width = 2409
integer height = 2056
integer taborder = 30
string title = "none"
string dataobject = "d_data_original"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_origin_raw from userobject within tab_origin
integer x = 18
integer y = 112
integer width = 2423
integer height = 2088
long backcolor = 16777215
string text = "Raw"
long tabtextcolor = 33554432
long tabbackcolor = 16777215
long picturemaskcolor = 16777215
mle_origin_raw mle_origin_raw
end type

on tabpage_origin_raw.create
this.mle_origin_raw=create mle_origin_raw
this.Control[]={this.mle_origin_raw}
end on

on tabpage_origin_raw.destroy
destroy(this.mle_origin_raw)
end on

type mle_origin_raw from multilineedit within tabpage_origin_raw
integer y = 20
integer width = 2409
integer height = 2056
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_origin_api_key from userobject within tab_origin
integer x = 18
integer y = 112
integer width = 2423
integer height = 2088
long backcolor = 16777215
string text = "Configuration"
long tabtextcolor = 33554432
long tabbackcolor = 16777215
long picturemaskcolor = 16777215
sle_api_key sle_api_key
st_api_key st_api_key
end type

on tabpage_origin_api_key.create
this.sle_api_key=create sle_api_key
this.st_api_key=create st_api_key
this.Control[]={this.sle_api_key,&
this.st_api_key}
end on

on tabpage_origin_api_key.destroy
destroy(this.sle_api_key)
destroy(this.st_api_key)
end on

type sle_api_key from singlelineedit within tabpage_origin_api_key
integer x = 507
integer y = 56
integer width = 1874
integer height = 88
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "AIzaSyA8KwMZ1ngxUi-XZHy7lqhbnucvo7oHDYs"
borderstyle borderstyle = stylelowered!
end type

event modified;//	Get the API Key from the Configuration SLE in the tab_origin
is_api_key	= TRIM(tab_origin.tabpage_origin_api_key.sle_api_key.text)

//	Check if there is an API Key
IF ISNULL(is_api_key) OR is_api_key = "" THEN
	//	Notify the user
	MessageBox("Warning!", "No Google API Key has been captured. This info is required in order to consume the Web Service.", StopSign!)
	
	//	Select the SLE for the API Key
	tab_origin.SelectTab(5)
	
	//	Set the focus on the SLE
	tab_origin.tabpage_origin_api_key.sle_api_key.SetFocus()
END IF
end event

type st_api_key from statictext within tabpage_origin_api_key
integer x = 32
integer y = 68
integer width = 503
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 553648127
string text = "Google API Key:"
boolean focusrectangle = false
end type

type gb_origin from groupbox within w_translate
boolean visible = false
integer width = 2578
integer height = 2340
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Origin"
end type

type gb_translation from groupbox within w_translate
boolean visible = false
integer x = 3232
integer width = 2578
integer height = 2340
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Translation"
end type

type gb_lang from groupbox within w_translate
integer x = 2601
integer y = 724
integer width = 613
integer height = 700
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 553648127
string text = "Languaje"
end type

type p_source from picture within w_translate
integer width = 2578
integer height = 2396
string picturename = "images\source.png"
boolean focusrectangle = false
end type

type p_translate from picture within w_translate
integer x = 3232
integer width = 2578
integer height = 2396
string picturename = "images\translation.png"
boolean focusrectangle = false
end type


﻿$PBExportHeader$n_webservice.sru
$PBExportComments$Generated Web service object
forward
global type n_webservice from nonvisualobject
end type
end forward

global type n_webservice from nonvisualobject descriptor "PB_ObjectCodeAssistants" = "{1E00F051-675A-11D2-BCA5-000086095DDA}" 
end type
global n_webservice n_webservice

forward prototypes
public function integer add (integer v1, integer v2)
public function integer sqr (integer v)
public function integer of_personal_num (ref long al_rows)
public function integer of_personal (ref s_personal arr[])
public function integer of_update (long key1, long key2, string val1, string val2, string val3)
end prototypes

public function integer add (integer v1, integer v2);return v1 + v2
end function

public function integer sqr (integer v);Return v * v
end function

public function integer of_personal_num (ref long al_rows);Integer li_rc = 0

// Profile ASPE
SQLCA.DBMS = "SNC SQL Native Client(OLE DB)"
SQLCA.LogPass = 'gxpower'
SQLCA.ServerName = "PBCURSO"
SQLCA.LogId = "gxpower"
SQLCA.AutoCommit = False
SQLCA.DBParm = "Database='ASPE',Provider='SQLNCLI11'"

CONNECT Using sqlca;

If sqlca.sqlcode = 0 Then
	Datastore lds
	lds = CREATE Datastore
	lds.DataObject = 'd_personal'
	li_rc = lds.SetTransObject(sqlca)
	
	If li_rc = 1 Then
		al_rows = lds.Retrieve()
	Else
		li_rc = -1
		//as_errmsg = 'DB error connection. SetTransObject not working'
	End If
	
	DESTROY lds
Else 
	li_rc = -1
	//as_errmsg = 'DB error connection. ' + sqlca.sqlErrText
End If

DISCONNECT Using sqlca;

Return li_rc
end function

public function integer of_personal (ref s_personal arr[]);Integer li_rc = 0

// Profile ASPE
SQLCA.DBMS = "SNC SQL Native Client(OLE DB)"
SQLCA.LogPass = 'gxpower'
SQLCA.ServerName = "PBCURSO"
SQLCA.LogId = "gxpower"
SQLCA.AutoCommit = False
SQLCA.DBParm = "Database='ASPE',Provider='SQLNCLI11'"

CONNECT Using sqlca;

If sqlca.sqlcode = 0 Then
	Datastore lds
	lds = CREATE Datastore
	lds.DataObject = 'd_personal'
	li_rc = lds.SetTransObject(sqlca)
	
	If li_rc = 1 Then
		lds.Retrieve()
		arr = lds.object.data
	Else
		li_rc = -1
		arr[1].nombre = 'DB error connection. SetTransObject not working'
	End If
	
	DESTROY lds
Else 
	li_rc = -1
	arr[1].nombre = 'DB error connection. ' + sqlca.sqlErrText
End If

DISCONNECT Using sqlca;

Return li_rc
end function

public function integer of_update (long key1, long key2, string val1, string val2, string val3);Integer li_rc = 0

// Profile ASPE
SQLCA.DBMS = "SNC SQL Native Client(OLE DB)"
SQLCA.LogPass = 'gxpower'
SQLCA.ServerName = "PBCURSO"
SQLCA.LogId = "gxpower"
SQLCA.AutoCommit = False
SQLCA.DBParm = "Database='ASPE',Provider='SQLNCLI11'"

CONNECT Using sqlca;

If sqlca.sqlcode = 0 Then
	// Update
	UPDATE personal SET apellid1 =:val1, apellid2 = :val2, nombre = :val3 
	WHERE codienti = :key1 and codiempl = :key2
	USING SQLCA;
	
	IF sqlca.sqlcode = 0 THEN
		COMMIT USING SQLCA;
	ELSE
		li_rc = -1
		ROLLBACK USING SQLCA;
	END IF
	
	
End If

DISCONNECT Using sqlca;

Return li_rc
end function

on n_webservice.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_webservice.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on


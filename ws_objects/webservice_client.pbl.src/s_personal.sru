﻿$PBExportHeader$s_personal.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type s_personal from nonvisualobject
    end type
end forward

global type s_personal from nonvisualobject
end type

type variables
    long codienti
    long codiempl
    string apellid1
    string apellid2
    string nombre
end variables

on s_personal.create
call super::create
TriggerEvent( this, "constructor" )
end on

on s_personal.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on


﻿$PBExportHeader$w_main.srw
forward
global type w_main from window
end type
type cb_10 from commandbutton within w_main
end type
type cb_9 from commandbutton within w_main
end type
type cb_8 from commandbutton within w_main
end type
type cb_7 from commandbutton within w_main
end type
type cb_6 from commandbutton within w_main
end type
type cb_5 from commandbutton within w_main
end type
type cb_4 from commandbutton within w_main
end type
type cb_3 from commandbutton within w_main
end type
type rmsg from commandbutton within w_main
end type
type cb_2 from commandbutton within w_main
end type
type dw_1 from datawindow within w_main
end type
type cb_1 from commandbutton within w_main
end type
type gb_1 from groupbox within w_main
end type
type gb_2 from groupbox within w_main
end type
type gb_3 from groupbox within w_main
end type
type gb_4 from groupbox within w_main
end type
end forward

global type w_main from window
integer width = 3378
integer height = 1808
boolean titlebar = true
string title = "WS Client Application"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_10 cb_10
cb_9 cb_9
cb_8 cb_8
cb_7 cb_7
cb_6 cb_6
cb_5 cb_5
cb_4 cb_4
cb_3 cb_3
rmsg rmsg
cb_2 cb_2
dw_1 dw_1
cb_1 cb_1
gb_1 gb_1
gb_2 gb_2
gb_3 gb_3
gb_4 gb_4
end type
global w_main w_main

on w_main.create
this.cb_10=create cb_10
this.cb_9=create cb_9
this.cb_8=create cb_8
this.cb_7=create cb_7
this.cb_6=create cb_6
this.cb_5=create cb_5
this.cb_4=create cb_4
this.cb_3=create cb_3
this.rmsg=create rmsg
this.cb_2=create cb_2
this.dw_1=create dw_1
this.cb_1=create cb_1
this.gb_1=create gb_1
this.gb_2=create gb_2
this.gb_3=create gb_3
this.gb_4=create gb_4
this.Control[]={this.cb_10,&
this.cb_9,&
this.cb_8,&
this.cb_7,&
this.cb_6,&
this.cb_5,&
this.cb_4,&
this.cb_3,&
this.rmsg,&
this.cb_2,&
this.dw_1,&
this.cb_1,&
this.gb_1,&
this.gb_2,&
this.gb_3,&
this.gb_4}
end on

on w_main.destroy
destroy(this.cb_10)
destroy(this.cb_9)
destroy(this.cb_8)
destroy(this.cb_7)
destroy(this.cb_6)
destroy(this.cb_5)
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.rmsg)
destroy(this.cb_2)
destroy(this.dw_1)
destroy(this.cb_1)
destroy(this.gb_1)
destroy(this.gb_2)
destroy(this.gb_3)
destroy(this.gb_4)
end on

event open;//ddfdfsd
end event

type cb_10 from commandbutton within w_main
integer x = 1024
integer y = 1536
integer width = 471
integer height = 112
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "JSON Generator"
end type

event clicked;JsonGenerator lnv_JsonGenerator
string ls_Json
lnv_JsonGenerator = create JsonGenerator

// Create an array root item
Long ll_RootArray 
ll_RootArray = lnv_JsonGenerator.CreateJsonArray()
	
// Add an Object child item
Long ll_ChildObject 
ll_ChildObject = lnv_JsonGenerator.AddItemObject(ll_RootArray)
lnv_JsonGenerator.AddItemNumber(ll_ChildObject, "year", 2017)
lnv_JsonGenerator.AddItemDate(ll_ChildObject, "date", 2017-09-21)
lnv_JsonGenerator.AddItemTime(ll_ChildObject, "time", 12:00:00)

// Add an array child item
Long ll_ChildArray 
ll_ChildArray = lnv_JsonGenerator.AddItemArray(ll_RootArray)
lnv_JsonGenerator.AddItemNumber(ll_ChildArray, 101)
lnv_JsonGenerator.AddItemNumber(ll_ChildArray, 102)
lnv_JsonGenerator.AddItemNumber(ll_ChildArray, 103)

// Gets the JSON string
ls_Json = lnv_JsonGenerator.GetJsonString()

MessageBox("JSON generator", ls_json)
end event

type cb_9 from commandbutton within w_main
integer x = 1024
integer y = 1408
integer width = 494
integer height = 112
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "GetResponse"
end type

event clicked;HttpClient lnv_HttpClient
lnv_HttpClient = Create HttpClient

// Send request using GET method
Integer li_rc, li_StatusCode
String ls_ContentType, ls_body

li_rc = lnv_HttpClient.SendRequest("GET", "http://jsonplaceholder.typicode.com/albums")
//li_rc = lnv_HttpClient.SendRequest("GET", "http://localhost/image.png")

// Obtain the response message
if li_rc = 1 then
	 // Obtain the response status
	 li_StatusCode = lnv_HttpClient.GetResponseStatusCode()
	 if li_StatusCode = 200 then
		  // Obtain the header
		  ls_ContentType = lnv_HttpClient.GetResponseHeader("Content-Type") // Obtain the specifid header
		  MessageBox("Header", ls_ContentType)
		  // Obtain the response data
		  lnv_HttpClient.GetResponseBody(ls_body) // No encoding is specified, because encoding of the response data is unknown
		  //lnv_HttpClient.GetResponseBody(ls_string, EncodingUTF8!) // Encoding of the response data is known to be EncodingUTF8!.
		  //lnv_HttpClient.GetResponseBody(lblb_blob) // Obtain the response data and convert to a blob
		  MessageBox("Body", ls_body)
		 end if
end if

end event

type cb_8 from commandbutton within w_main
integer x = 183
integer y = 1504
integer width = 402
integer height = 112
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Albums"
end type

event clicked;Long ll_rc
RestClient lnv_RestClient
lnv_RestClient = Create RestClient

// Set DataObject
dw_1.DataObject = "d_json_albums"

// Send request using GET
ll_rc = lnv_RestClient.Retrieve(dw_1, "http://jsonplaceholder.typicode.com/albums")

// Check the return value
if ll_rc >= 0 then
	MessageBox("Success", "Rows = " + String(ll_rc))
else
	MessageBox("Error", "Failed to retrieve data.")
end if

end event

type cb_7 from commandbutton within w_main
integer x = 183
integer y = 1376
integer width = 402
integer height = 112
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Employees"
end type

event clicked;Long ll_rc
RestClient lnv_RestClient
lnv_RestClient = Create RestClient

// Set DataObject
dw_1.DataObject = "d_json_employees"

// Send request using GET
ll_rc = lnv_RestClient.Retrieve(dw_1, "http://localhost/employees.json")

// Check the return value
if ll_rc >= 0 then
	MessageBox("Success", "Rows = " + String(ll_rc))
else
	MessageBox("Error", "Failed to retrieve data.")
end if

end event

type cb_6 from commandbutton within w_main
integer x = 110
integer y = 1088
integer width = 622
integer height = 96
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Update"
end type

event clicked;dw_1.Update()
end event

type cb_5 from commandbutton within w_main
integer x = 110
integer y = 384
integer width = 622
integer height = 96
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "of_Personal"
end type

event clicked;Integer li_rc
//Long ll_num = 0

String ls_errmsg = ''
s_personal la_personal[]
n_webservice myproxy

SoapConnection conn
conn = CREATE SoapConnection

li_rc = conn.CreateInstance(myproxy, "n_webservice")

If li_rc = 0 Then
	li_rc = myproxy.of_personal(la_personal)
	
	MessageBox("WS Result", "Number of items " + String(la_personal[1].nombre))
	
	MessageBox("WS Result", "Number of items " + String(UpperBound(la_personal)))
Else
	MessageBox("Error", "Cannot create an instance of the proxy object", Exclamation!)
End If

DESTROY conn
end event

type cb_4 from commandbutton within w_main
integer x = 110
integer y = 960
integer width = 622
integer height = 96
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "of_Personal"
end type

event clicked;
dw_1.DataObject = "d_webservice_personal"

dw_1.Retrieve()
end event

type cb_3 from commandbutton within w_main
integer x = 110
integer y = 832
integer width = 622
integer height = 96
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "of_Personal_num"
end type

event clicked;
dw_1.DataObject = "d_webservice_personal_num"

dw_1.Retrieve()
end event

type rmsg from commandbutton within w_main
integer x = 110
integer y = 256
integer width = 622
integer height = 96
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "of_Personal_num"
end type

event clicked;Integer li_rc
Long ll_num = 0
String ls_errmsg = ''
n_webservice myproxy

SoapConnection conn
conn = CREATE SoapConnection

li_rc = conn.CreateInstance(myproxy, "n_webservice")

If li_rc = 0 Then
	li_rc = myproxy.of_personal_num(ll_num)
	MessageBox("WS Result", "Number of people is " + String(ll_num))
Else
	MessageBox("Error", "Cannot create an instance of the proxy object", Exclamation!)
End If

DESTROY conn
end event

type cb_2 from commandbutton within w_main
integer x = 110
integer y = 704
integer width = 622
integer height = 96
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Sum"
end type

event clicked;dw_1.DataObject = 'd_webservice_add'

dw_1.Retrieve(11, 22)
end event

type dw_1 from datawindow within w_main
integer x = 805
integer y = 64
integer width = 2487
integer height = 1120
integer taborder = 20
string title = "none"
string dataobject = "d_webservice_add"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_main
integer x = 110
integer y = 128
integer width = 622
integer height = 96
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Sqr"
end type

event clicked;Integer li_rc, li_sum = 0, li_sqr = 0
n_webservice myproxy

SoapConnection conn
conn = CREATE SoapConnection

li_rc = conn.CreateInstance(myproxy, "n_webservice")

If li_rc = 0 Then
	//li_sum = myproxy.Add(4, 7)
	li_sqr = myproxy.Sqr(4)
	MessageBox("WS Result", "Sum is " + String(li_sqr))
Else
	MessageBox("Error", "Cannot create an instance of the proxy object", Exclamation!)
End If

DESTROY conn


end event

type gb_1 from groupbox within w_main
integer x = 37
integer y = 64
integer width = 768
integer height = 448
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "PROXY"
end type

type gb_2 from groupbox within w_main
integer x = 37
integer y = 576
integer width = 768
integer height = 672
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "WS DW"
end type

type gb_3 from groupbox within w_main
integer x = 37
integer y = 1280
integer width = 768
integer height = 384
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "RESTClient"
end type

type gb_4 from groupbox within w_main
integer x = 914
integer y = 1280
integer width = 768
integer height = 384
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "HTTPClient"
end type


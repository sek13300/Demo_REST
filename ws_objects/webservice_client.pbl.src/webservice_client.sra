﻿$PBExportHeader$webservice_client.sra
$PBExportComments$Generated Application Object
forward
global type webservice_client from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global type webservice_client from application
string appname = "webservice_client"
end type
global webservice_client webservice_client

on webservice_client.create
appname="webservice_client"
message=create message
sqlca=create transaction
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on webservice_client.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;open(w_main)
end event

